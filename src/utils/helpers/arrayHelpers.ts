export const sortArrayBy = <T>(array: T[], by: MatchNumber<T>): T[] => {
  return array.sort((a, b) => {
    return +b[by] - +a[by];
  });
};

export const getMostPopular = <T>(array: T[], by: MatchNumber<T>, itemsCount: number): T[] => {
  return sortArrayBy(array, by).slice(0, itemsCount);
};

type MatchNumber<T> = { [K in keyof T]-?: T[K] extends number ? K : never }[keyof T];
