interface SuccessResponse<T> {
  status: 'success';
  data: T;
}

interface ErrorResponse {
  status: 'failure';
  error: {
    status: string;
    message: string;
    data: any;
  };
}

type Response<T> = SuccessResponse<T> | ErrorResponse;

export const ParseToErrorResponse = (error: any): ErrorResponse => {
  return { status: 'failure', error: { ...error } };
};

export const ParseToSuccessResponse = <T>(data: T): SuccessResponse<T> => {
  return { status: 'success', data };
};

export default Response;
