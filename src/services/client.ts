import axios, { AxiosError, Canceler } from 'axios';
import history from 'utils/history';

const CancelToken = axios.CancelToken;
export const source = CancelToken.source();
export let cancel: Canceler;

const baseURL = 'https://api.github.com';

const client = axios.create({
  baseURL,
  headers: {
    'Content-Type': 'application/json',
  },
});

client.interceptors.request.use((config) => {
  return {
    ...config,
    cancelToken: new CancelToken(function executor(c) {
      cancel = c;
    }),
  };
});

client.interceptors.response.use(
  (response) => {
    return response;
  },
  (error: AxiosError) => {
    if (error.response === undefined) {
      history.push('/500');
      cancel();
    }
    switch (error.response?.status) {
      case 500:
        history.push('/500');
        break;
      default:
        break;
    }
    return Promise.reject(error.response);
  }
);

export default client;
