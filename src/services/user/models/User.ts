import UserRepos from './UserRepos';

interface User {
  id: number;
  avatar_url: string;
  bio: string | null;
  name: string | null;
}

interface UserWithRepos extends User {
  repos: UserRepos[];
}

export default UserWithRepos;
