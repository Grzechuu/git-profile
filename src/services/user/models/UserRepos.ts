interface UserRepos {
  watchers: number;
  name: string;
  id: number;
  html_url: string;
}

export default UserRepos;
