import client from 'services/client';
import Response, { ParseToSuccessResponse, ParseToErrorResponse } from 'services/app/models/Envelope';
import User from './models/User';
import UserRepos from './models/UserRepos';
import Axios from 'axios';
import UserWithRepos from './models/User';
import { getMostPopular } from 'utils/helpers/arrayHelpers';

const CancelToken = Axios.CancelToken;
export const source = CancelToken.source();

export const fetchUser = async (username: string): Promise<Response<UserWithRepos>> => {
  try {
    const responseUser = await client.get<User>(`/users/${username}`, { cancelToken: source.token });
    const responseRepos = await client.get<UserRepos[]>(`/users/${username}/repos`, { cancelToken: source.token });
    return ParseToSuccessResponse({ ...responseUser.data, repos: getMostPopular(responseRepos.data, 'watchers', 3) });
  } catch (error) {
    return ParseToErrorResponse(error);
  }
};
