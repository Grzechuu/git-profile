import React from 'react';
import './App.scss';

import { Route, Switch } from 'react-router-dom';

import Searchbar from 'views/user/Searchbar';
import UserProvider from 'contexts/UserContext';
import ErrorPage from 'views/app/ErrorPage';
import UserPage from 'views/user/UserPage';

const App: React.FC = () => {
  return (
    <main className="container">
      <UserProvider>
        <Searchbar />
        <Switch>
          <Route exact path={'/user/:username'} component={UserPage} />
          <Route exact path="/500" component={ErrorPage} />
        </Switch>
      </UserProvider>
    </main>
  );
};

export default App;
