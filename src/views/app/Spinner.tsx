import React from 'react';
import ClipLoader from 'react-spinners/ClipLoader';

const Spinner: React.FC = () => {
  return (
    <div className="spinner">
      <ClipLoader size={100} />
    </div>
  );
};

export default Spinner;
