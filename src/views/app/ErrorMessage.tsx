import React from 'react';

interface Props {
  message: string | null;
}

const ErrorMessage: React.FC<Props> = ({ message }) => {
  return message ? <span className="error-message">{message}</span> : null;
};

export default ErrorMessage;
