import React from 'react';
import User from 'services/user/models/User';
import TrendingRepo from './TrendingRepo';

interface Props {
  user: User;
}

const UserDetails: React.FC<Props> = (props) => {
  const { user } = props;
  return (
    <div className="details-container">
      <div className="meta-data">
        <img src={user.avatar_url} />
        <span className="title">{user.name}</span>
        <span className="meta-data__bio">{user.bio}</span>
      </div>
      <div className="repositories">
        <span className="title">Top Repositories</span>
        {user.repos.map((repo) => (
          <TrendingRepo key={repo.id} name={repo.name} repoUrl={repo.html_url} />
        ))}
      </div>
    </div>
  );
};

export default UserDetails;
