import React from 'react';

interface Props {
  name: string;
  repoUrl: string;
}

const TrendingRepo: React.FC<Props> = ({ name, repoUrl }) => {
  return (
    <div className="repositories__item">
      <a href={repoUrl}>{name}</a>
    </div>
  );
};

export default TrendingRepo;
