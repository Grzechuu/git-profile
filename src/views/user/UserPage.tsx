import React, { useContext, useEffect } from 'react';

import { useParams } from 'react-router-dom';
import UserDetails from './UserDetails';
import Spinner from 'views/app/Spinner';
import ErrorMessage from 'views/app/ErrorMessage';
import { UserContext } from 'contexts/UserContext';

const UserPage: React.FC = () => {
  const { username } = useParams<{ username: string }>();
  const { isLoading, user, error, setParam } = useContext(UserContext);
  useEffect(() => {
    setParam(username);
  }, [username, setParam]);

  return <>{isLoading ? <Spinner /> : user ? <UserDetails user={user} /> : <ErrorMessage message={error} />}</>;
};

export default UserPage;
