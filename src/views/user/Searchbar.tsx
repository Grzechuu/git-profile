import React, { useContext } from 'react';
import { useFormik } from 'formik';
import { ReactComponent as Loupe } from 'assets/Search.svg';
import { UserContext } from 'contexts/UserContext';

const Searchbar: React.FC = () => {
  const formik = useFormik({
    initialValues: {
      username: '',
    },
    onSubmit: ({ username }) => {
      username && handleUserFetch(username);
    },
  });
  const { handleUserFetch } = useContext(UserContext);
  return (
    <>
      <nav className="header" />
      <form className="search-form" onSubmit={formik.handleSubmit}>
        <div className="search-field">
          <input
            type="text"
            className="search-field__input"
            placeholder="Search for users"
            name="username"
            value={formik.values.username}
            onChange={formik.handleChange}
            required
          />
          <Loupe className="search-field__icon" />
        </div>
        <button className="search-form__button" type="submit">
          Search
        </button>
      </form>
    </>
  );
};

export default Searchbar;
