import React from 'react';
import { render, screen } from '@testing-library/react';
import { UserContext } from 'contexts/UserContext';
import UserPage from 'views/user/UserPage';
import { createMemoryHistory } from 'history';
import { Router, Route } from 'react-router-dom';

function renderWithRouterAndRoute(
  ui: JSX.Element,
  { route = '/', path = '/', providerProps, history = createMemoryHistory({ initialEntries: [route] }) } = {}
) {
  const Wrapper: React.FC = ({ children }) => (
    <UserContext.Provider value={providerProps}>
      <Router history={history}>
        <Route path={path}>{children}</Route>
      </Router>
    </UserContext.Provider>
  );
  return {
    ...render(ui, { wrapper: Wrapper }),
    history,
  };
}

describe('user page', () => {
  it('should render spinner', async () => {
    const providerProps = {
      isLoading: true,
      setParam: jest.fn(),
    };
    const { container } = renderWithRouterAndRoute(<UserPage />, {
      route: '/user/username',
      path: '/user/:username',
      providerProps,
    });
    const spinner = container.querySelector('.spinner');
    expect(spinner).toBeInTheDocument();
  });
  it('should render error message', async () => {
    const providerProps = {
      isLoading: false,
      error: 'test error message',
      setParam: jest.fn(),
    };
    renderWithRouterAndRoute(<UserPage />, {
      route: '/user/username',
      path: '/user/:username',
      providerProps,
    });
    const error = screen.getByText(providerProps.error);
    expect(error).toBeInTheDocument();
  });
  it('should trigger setParam', async () => {
    const providerProps = {
      setParam: jest.fn(),
    };
    renderWithRouterAndRoute(<UserPage />, {
      route: '/user/username',
      path: '/user/:username',
      providerProps,
    });
    expect(providerProps.setParam).toBeCalledTimes(1);
  });
});
