import React from 'react';
import { render } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';

import App from 'App';

function renderWithRouter(
  ui: JSX.Element,
  { route = '/', history = createMemoryHistory({ initialEntries: [route] }) } = {}
) {
  const Wrapper: React.FC = ({ children }) => <Router history={history}>{children}</Router>;
  return {
    ...render(ui, { wrapper: Wrapper }),
    history,
  };
}
describe('App', () => {
  it('renders error page with search bar', () => {
    const route = '/500';
    const { getByText, getByPlaceholderText } = renderWithRouter(<App />, { route });
    const text = getByText('Wierd... Works for me');
    const inputText = getByPlaceholderText('Search for users');
    const buttonText = getByText('Search');
    expect(text).toBeInTheDocument();
    expect(inputText).toBeInTheDocument();
    expect(buttonText).toBeInTheDocument();
  });
  it('renders searchbar on every path', () => {});
});
