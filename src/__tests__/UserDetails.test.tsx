import React from 'react';
import { render } from '@testing-library/react';
import UserDetails from 'views/user/UserDetails';
import { fetchUser } from '__mocks__/services/user/userService';

const user = {
  id: 1,
  avatar_url: 'https://avatar/',
  name: 'test',
  bio: 'bio test',
  repos: [
    {
      id: 1,
      name: 'repo',
      html_url: 'url1',
      watchers: 3,
    },
    {
      id: 2,
      name: 'repo',
      html_url: 'url2',
      watchers: 2,
    },
    {
      id: 3,
      name: 'repo',
      html_url: 'url3',
      watchers: 1,
    },
  ],
};

describe('user page', () => {
  beforeAll(() => {
    fetchUser.mockReturnValue({ status: 'success', data: user });
  });
  it('should display user', async () => {
    const { getByText, getAllByText, container } = render(<UserDetails user={user} />);
    const img = container.querySelector('img')?.src;
    const name = getByText(user.name);
    const bio = getByText(user.bio);
    const repos = getAllByText('repo');
    expect(img).toBe(user.avatar_url);
    expect(bio).toBeInTheDocument();
    expect(name).toBeInTheDocument();
    expect(repos).toHaveLength(3);
  });
});
