import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import Searchbar from 'views/user/Searchbar';
import UserProvider, { UserContext } from 'contexts/UserContext';
import history from 'utils/history';
import { fetchUser } from '__mocks__/services/user/userService';

const customRender = (ui, { providerProps, ...renderOptions }) => {
  return render(<UserContext.Provider value={providerProps}>{ui}</UserContext.Provider>, renderOptions);
};

describe('searchbar form', () => {
  beforeAll(() => {
    fetchUser.mockReturnValue({ status: 'success', data: {} });
  });
  it('should not submit blank form', async () => {
    const providerProps = {
      handleUserFetch: jest.fn(),
    };
    const { getByRole } = customRender(<Searchbar />, { providerProps });
    const button = getByRole('button');
    await waitFor(() => {
      fireEvent.click(button);
    });
    expect(providerProps.handleUserFetch).toBeCalledTimes(0);
  });
  it('should submit form', async () => {
    const providerProps = {
      handleUserFetch: jest.fn(),
    };
    const { getByRole } = customRender(<Searchbar />, { providerProps });
    const input = getByRole('textbox');
    fireEvent.change(input, {
      target: { value: 'username' },
    });
    const button = getByRole('button');
    await waitFor(() => {
      fireEvent.click(button);
    });
    expect(providerProps.handleUserFetch).toBeCalledTimes(1);
  });
  it('should change route on form submit', async () => {
    const { getByRole } = render(
      <UserProvider>
        <Searchbar />
      </UserProvider>
    );
    const input = getByRole('textbox');
    fireEvent.change(input, {
      target: { value: 'username' },
    });
    const button = getByRole('button');
    await waitFor(() => {
      fireEvent.click(button);
    });
    expect(history.location.pathname).toBe('/user/username');
  });
});
