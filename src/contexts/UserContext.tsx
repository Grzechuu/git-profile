import React, { useState, useEffect, useCallback } from 'react';
import { fetchUser } from 'services/user/userService';
import UserWithRepos from 'services/user/models/User';
import history from 'utils/history';

export interface UserContextInterface {
  user: UserWithRepos | null;
  isLoading: boolean;
  error: string | null;
  handleUserFetch: (username: string) => void;
  setParam: (path: string) => void;
}

export const UserContext = React.createContext<UserContextInterface>({
  user: null,
  error: null,
  isLoading: false,
  handleUserFetch: () => {},
  setParam: () => {},
});

const UserProvider: React.FC = (props) => {
  const [param, setParam] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [user, setUser] = useState<UserWithRepos | null>(null);
  const [error, setError] = useState<string | null>(null);

  const shouldPushToHistory = useCallback(
    (username: string) => {
      if (param !== username) {
        history.push(`/user/${username}`);
      }
    },
    [param]
  );

  const handleUserFetch = useCallback(
    async (username: string) => {
      setIsLoading(true);
      setError(null);
      const response = await fetchUser(username);
      if (response.status === 'success') {
        setUser(response.data);
        setIsLoading(false);
      } else {
        setError(response.error.data.message);
        setUser(null);
        setIsLoading(false);
      }
      shouldPushToHistory(username);
    },
    [shouldPushToHistory]
  );

  useEffect(() => {
    if (param) {
      handleUserFetch(param);
    }
  }, [param, handleUserFetch]);

  return (
    <UserContext.Provider
      value={{
        user: user,
        error: error,
        isLoading,
        handleUserFetch,
        setParam,
      }}
    >
      {props.children}
    </UserContext.Provider>
  );
};

export default UserProvider;
